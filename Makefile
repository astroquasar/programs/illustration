# Makefile for qsosim9
# JKW, Dec 2013
# Compiler:
FC= gfortran

# List of executables to be built within the package

# this is the default (ie 'make' will make for Linux as below)
#all: $(PROGRAMS)
all: q9S

obj=	dsepvar.o ewred.o spvoigt.o voigt.o vp_lycont.o f13_read.o \

#  Macbook Pro version
q9S: qsosim9S.o $(obj)
	$(LINK.f) -o qsosim9S qsosim9S.o $(obj) \
	-L/opt/local/lib -lpgplot -L/usr/X11/lib -lX11	
